'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const { requestLogger, errorLogger } = require('../config/logger');

module.exports = (db) => {

    /**
  * @swagger
  * /health: 
  *  get:
  *    description: Use to get the health of the API
  *    responses:
  *      '200':
  *        description: Healthy
  */

    app.get('/health', (req, res) => res.send('Healthy'));


    /**
     * @swagger
     *
     * /rides:
     *   post:
     *     tags:
     *       - Rides
     *     description: Create rides
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: ridesData
     *         description: Object of fields need for this
     *         in: body
     *         required: true
     *         schema:
     *           properties:
     *             start_lat:
     *               type: integer
     *             start_long:
     *               type: integer
     *             end_lat:
     *               type: integer
     *             end_long:
     *               type: integer
     *             rider_name:
     *               type: string
     *             driver_name:
     *               type: string
     *             driver_vehicle:
     *               type: string
     *     responses:
     *       200:
     *         description: Rides created successfull
     *       403: 
     *         description: You do not have necessary permissions to get token
     */


    app.post('/rides', jsonParser, async (req, res) => {
        const startLatitude = Number(req.body.start_lat);
        const startLongitude = Number(req.body.start_long);
        const endLatitude = Number(req.body.end_lat);
        const endLongitude = Number(req.body.end_long);
        const riderName = req.body.rider_name;
        const driverName = req.body.driver_name;
        const driverVehicle = req.body.driver_vehicle;


        if (startLatitude < -90 ||
            startLatitude > 90 ||
            startLongitude < -180 ||
            startLongitude > 180
        ) {
            return res.send({
                error_code: 'VALIDATION_ERROR',
                message: 'Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
            });
        }

        if (endLatitude < -90 ||
            endLatitude > 90 ||
            endLongitude < -180 ||
            endLongitude > 180
        ) {
            return res.send({
                error_code: 'VALIDATION_ERROR',
                message: 'End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
            });
        }
        if (typeof riderName !== 'string' || riderName.length < 1) {
            return res.send({
                error_code: 'VALIDATION_ERROR',
                message: 'Rider name must be a non empty string'
            });
        }

        if (typeof driverName !== 'string' || driverName.length < 1) {
            return res.send({
                error_code: 'VALIDATION_ERROR',
                message: 'Rider name must be a non empty string'
            });
        }

        if (typeof driverVehicle !== 'string' || driverVehicle.length < 1) {
            return res.send({
                error_code: 'VALIDATION_ERROR',
                message: 'Driver vehicle name must be a non empty string'
            });
        }

        let values = [
            req.body.start_lat,
            req.body.start_long,
            req.body.end_lat,
            req.body.end_long,
            req.body.rider_name,
            req.body.driver_name,
            req.body.driver_vehicle
        ];

        try {
            const result = await db.run(
                `INSERT INTO Rides(
                    startLat, 
                    startLong, 
                    endLat,
                    endLong, 
                    riderName, 
                    driverName, 
                    driverVehicle
                    ) 
                    VALUES (?, ?, ?, ?, ?, ?, ?)`,
                values
            )
            if (result.changes) {
                const insertedRecord = await db.all(
                    "SELECT * FROM Rides WHERE rideID = ?",
                    result.lastID
                );
                requestLogger.info(`Rides data successfully inserted for id = ${result.lastID}`)
                res.send({ "success": true, data: insertedRecord })
            }

        } catch (e) {
            errorLogger.info(`Someting went wrong ${e}`);
            return res.send({
                error_code: 'SERVER_ERROR',
                message: 'Unknown error'
            });
        }

    });

    /**
    * @swagger
    * /rides:
    *  get:
    *    description: Get all rides
    *
    *    responses:
    *      '200':
    *        description: Rides List
    */


    app.get('/rides', async (req, res) => {
        const { limit, offset } = req.query;
        let query = '';
        if (limit != undefined && offset != undefined) {
            query = `SELECT * FROM Rides LIMIT ${limit} OFFSET ${offset}`
        }
        else {
            query = `SELECT * FROM Rides`;
        }
        try {
            const rides = await db.all(query);
            if (rides.length === 0) {
                errorLogger.error(`Could not find any rides`);
                return res.send({
                    error_code: "RIDES_NOT_FOUND_ERROR",
                    message: "Could not find any rides",
                });
            }
            res.send(rides);
        } catch (error) {
            errorLogger.error(`Someting went wrong ${error}`);
            return res.send({
                error_code: "SERVER_ERROR",
                message: "Unknown error",
            });
        }
    });


    /**
* @swagger
* /rides/{id}:
*  get:
*    description: Use to get a single ride from the API
*    parameters:
*    - in: path
*      name: id
*      schema:
*         type: integer
*         required: true
*    responses:
*      '200':
*        description: A Single Ride
*/

    app.get('/rides/:id', async (req, res) => {

        try {

            const result = await db.all(`SELECT * 
                 FROM Rides 
                 WHERE rideID = ?`,
                `${req.params.id}`)

            if (result.length === 0) {
                errorLogger.error(`Could not find any rides for id ${req.params.id}}`);
                return res.send({
                    error_code: "RIDES_NOT_FOUND_ERROR",
                    message: "Could not find any rides",
                });
            }

            res.send({ "success": true, data: result });

        } catch (error) {
            errorLogger.error(`Someting went wrong ${error}`);
            return res.send({
                error_code: 'RIDES_NOT_FOUND_ERROR',
                message: 'Could not find any rides'
            });

        }


    });

    return app;
};
