'use strict';

const express = require('express');
const app = express();
const port = 8010;
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const sqlite3 = require('sqlite3').verbose();

const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const Database = require("sqlite-async");
const db = new sqlite3.Database(':memory:');

const buildSchemas = require('./src/schemas');

const swaggerOptions = {
  swaggerDefinition: {
    components: {},
    info: {
      version: "1.0.0",
      title: "Rides API",
      description: "Xendit Rides API",
      contact: {
        name: "Tauqeer Software Engineer",
      },
      servers: [`http://localhost:8010`],
    },
  },
  // ['.routes/*.js']
  apis: [`${__dirname}/src/app.js`],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);



Database.open(":memory:")
  .then((db) => {
    buildSchemas(db);

    const app = require('./src/app')(db);
    app.get("/", ((req, res) => {
      res.json("Xendit Assesment")
    }))
    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
    app.listen(port, () => console.log(`App started and listening on port ${port}`));
  });
