const { requestLogger, errorLogger } = require('../config/logger');

const { exec } = require('child_process');


const main = () => {
    exec('artillery quick --count 100 -n 20 http:localhost:8010/health', (err, stdout, stderr) => {
        if (err) {
          errorLogger.error('Artillery => node could not execute the command for check health')
          console.log(err)
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        requestLogger.info(`Artillery => ${stdout}`)
        console.log(`stderr: ${stderr}`);
      });

      exec('artillery quick --count 100 -n 20 http:localhost:8010/rides', (err, stdout, stderr) => {
        if (err) {
          errorLogger.error('Artillery for test load health => node could not execute the command for check get All Rides')
          console.log(err)
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        requestLogger.info(`Artillery for test load get all rides => ${stdout}`)
        console.log(`stderr: ${stderr}`);
      });
}

main()