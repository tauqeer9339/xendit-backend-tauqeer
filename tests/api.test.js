'use strict';

const request = require('supertest');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

const app = require('../src/app')(db);
const buildSchemas = require('../src/schemas');

describe('API tests', () => {
    before((done) => {
        db.serialize((err) => {
            if (err) {
                return done(err);
            }

            buildSchemas(db);

            done();
        });
    });

    describe('GET /health', () => {
        it('should return health', (done) => {
            request(app)
                .get('/health')
                .expect('Content-Type', /text/)
                .expect(200, done);
        });
    });

    describe('POST /rides', () => {
        it('should return 200', () => {
            request(app)
                .post(`/rides`)
                .send({
                    "rider_name": "test",
                    "driver_name": "New test",
                    "driver_vehicle": "testt",
                    "start_lat": 90,
                    "start_long": 120,
                    "end_lat": 80,
                    "end_long": 170
                })
                .expect(200);
        });
    });

    describe('GET /rides', () => {
        it('should return all rides', (done) => {
            request(app)
                .get('/rides')
                .expect(200, done);
        });
    });

    describe('GET /rides/:id', () => {
        it('should return a ride', (done) => {
            request(app)
                .get(`/rides/${id}`)
                .expect(200, done);
        });
    });


});